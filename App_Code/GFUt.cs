﻿using System.Data;
using System.Text;
public static class GFUt {
    public static string[] colores = { "76,0,156", "0,128,255", "156,0,76", "255,51,51", "102,255,102", "255,255,102", "102,255,255", "255,102,255", "102,55,102" };
    public static string json_barras(DataTable dt) {
        string ret = "";
        string labels = "";
        string [] series = new string [dt.Columns.Count-1];
        string [] nseries = new string [dt.Columns.Count-1];
        for(int i = 0; i< dt.Rows.Count;i++){
            if(i!=0) labels+=",";
            labels += "\"" + dt.Rows[i][0].ToString() + "\"";
            for(int j = 0; j < series.Length; j++){
                if(i==0) { 
                    series[j] ="";
                    nseries[j] = "\"" + dt.Columns[j + 1].ColumnName + "\"";
                }
                else series[j]+=",";
                series[j]+=dt.Rows[i][j+1].ToString();
            }
        }
        ret += "{\"labels\":[" + labels + "],\"datasets\":[";
        for (int i = 0; i< series.Length;i++){
            if(i!=0) ret+=",";
            ret += "{\"label\":" + nseries[i] + ", " +
                    "\"fillColor\":\"rgba(" + colores[i % colores.Length] + ",0.75)\", " +
                    "\"strokeColor\":\"rgba(" + colores[i % colores.Length] + ",0.8)\", " +
                    "\"highlightFill\":\"rgba(" + colores[i % colores.Length] + ",0.5)\", " +
                    "\"highlightStroke\":\"rgba(" + colores[i % colores.Length] + ",0.8)\", " +
                    "\"data\":[" + series[i] + "]}";
        }
        ret+="]}";
        return ret;
    }
    public static string json_pie_h(DataTable dt) {
        string ret = "";

        ret += "[";
        for (int i = 0; i < dt.Columns.Count; i++) {
            if (i != 0) ret += ",";
            ret += "{\"label\":\"" + dt.Columns[i].ColumnName + "\", " +
                    "\"color\":\"rgba(" + colores[i % colores.Length] + ",0.75)\", " +
                    "\"highlight\":\"rgba(" + colores[i % colores.Length] + ",0.8)\", " +
                    "\"value\":" + dt.Rows[0][i] + "}";
        }
        ret += "]";
        return ret;
    }
    public static string json_pie_v(DataTable dt) {
        string ret = "";

        ret += "[";
        for (int i = 0; i < dt.Rows.Count; i++) {
            if (i != 0) ret += ",";
            ret += "{\"label\":\"" + dt.Rows[i][0] + "\", " +
                    "\"color\":\"rgba(" + colores[i % colores.Length] + ",0.75)\", " +
                    "\"highlight\":\"rgba(" + colores[i % colores.Length] + ",0.8)\", " +
                    "\"value\":" + dt.Rows[i][1] + "}";
        }
        ret += "]";
        return ret;
    }
    public const string colores_v2_01 = "\"#FDB45C\",\"#F7464A\",\"#4D5360\",\"#46BFBD\",\"#949FB1\",\"#9933ff\",\"#0099ff\"";
    public const string colores_v2_02 = "\"rgba(76,0,156,0.75)\",\"rgba(0,128,255,0.75)\", \"rgba(156,0,76,0.75)\", \"rgba(255,51,51,0.75)\", \"rgba(102,255,102,0.75)\", \"rgba(255,255,102,0.75)\", \"rgba(102,255,255,0.75)\", \"rgba(255,102,255,0.75)\", \"rgba(102,55,102,0.75)\"";
    public static string json_pie_V2(DataTable dt) {
        string ret = "";
        string labels = "";
        string[] series = new string[dt.Columns.Count - 1];
        string[] nseries = new string[dt.Columns.Count - 1];
        for (int i = 0; i < dt.Rows.Count; i++) {
            if (i != 0) labels += ",";
            labels += "\"" + dt.Rows[i][0].ToString() + "\"";
            for (int j = 0; j < series.Length; j++) {
                if (i == 0) {
                    series[j] = "";
                    nseries[j] = "\"" + dt.Columns[j + 1].ColumnName + "\"";
                } else series[j] += ",";
                series[j] += dt.Rows[i][j + 1].ToString();
            }
        }
        ret += "{\"labels\":[" + labels + "],\"datasets\":[";
        for (int i = 0; i < series.Length; i++) {
            if (i != 0) ret += ",";
            ret += "{\"backgroundColor\":[" + colores_v2_02 + "], " +
                    "\"data\":[" + series[i] + "]}";
        }
        ret += "]}";
        return ret;
    }
    public static string json_pie_V2(DataTable dt, string color) {
        string ret = "";
        string labels = "";
        string[] series = new string[dt.Columns.Count - 1];
        string[] nseries = new string[dt.Columns.Count - 1];
        for (int i = 0; i < dt.Rows.Count; i++) {
            if (i != 0) labels += ",";
            labels += "\"" + dt.Rows[i][0].ToString() + "\"";
            for (int j = 0; j < series.Length; j++) {
                if (i == 0) {
                    series[j] = "";
                    nseries[j] = "\"" + dt.Columns[j + 1].ColumnName + "\"";
                } else series[j] += ",";
                series[j] += dt.Rows[i][j + 1].ToString();
            }
        }
        ret += "{\"labels\":[" + labels + "],\"datasets\":[";
        for (int i = 0; i < series.Length; i++) {
            if (i != 0) ret += ",";
            ret += "{\"backgroundColor\":[" + color + "], " +
                    "\"data\":[" + series[i] + "]}";
        }
        ret += "]}";
        return ret;
    }
    public static string json_bar_V2(DataTable dt) {
        string ret = "";
        string labels = "";
        string[] series = new string[dt.Columns.Count - 1];
        string[] nseries = new string[dt.Columns.Count - 1];
        for (int i = 0; i < dt.Rows.Count; i++) {
            if (i != 0) labels += ",";
            labels += "\"" + dt.Rows[i][0].ToString() + "\"";
            for (int j = 0; j < series.Length; j++) {
                if (i == 0) {
                    series[j] = "";
                    nseries[j] = "\"" + dt.Columns[j + 1].ColumnName + "\"";
                } else series[j] += ",";
                series[j] += dt.Rows[i][j + 1].ToString();
            }
        }
        ret += "{\"labels\":[" + labels + "],\"datasets\":[";
        for (int i = 0; i < series.Length; i++) {
            if (i != 0) ret += ",";
            ret += "{\"backgroundColor\":\"rgba(" + colores[i % colores.Length] + ",0.75)\", " +
                    "\"label\":" +  nseries[i] + ", " +
                    "\"data\":[" + series[i] + "]}";
        }
        ret += "]}";
        return ret;
    }
}