﻿using System;
using System.Data.SqlClient;
using System.Data;
using System.Data.OleDb;
using System.Text;
using ClosedXML.Excel;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.IO;
/// <summary>
/// Descripción breve de BDUt
/// </summary>
public static class BDUt
{
    //private static string str_conn = ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
    public const string OK = "OK";

    public static DataTable ejecutar_sp_dt(String comando, String[] nom_par, Object[] val_par, string str_conn)
    {
        DataTable ret = new DataTable();
        SqlConnection conn = null;
        SqlCommand cmd = null;
        try
        {
            conn = new SqlConnection(str_conn);
            cmd = new SqlCommand(comando, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 3600 * 2;
            if (nom_par != null && val_par != null)
                for (int i = 0; i < Math.Min(nom_par.Length, val_par.Length); i++)
                    cmd.Parameters.AddWithValue(nom_par[i], val_par[i]);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            conn.Open();
            da.Fill(ret);
            conn.Close();
            da.Dispose();
            cmd.Dispose();
            conn.Dispose();
        }
        catch (Exception e)
        {
            //throw e;
           
        }
        finally {

            if (cmd != null)
                cmd.Dispose();
            if (conn != null)
            {
                conn.Close();
                conn.Dispose();
            }
        
        
        }
        return ret;
    }

    public static DataTable ejecutar_sp_dt(String comando, String nom_par, Object val_par, string str_conn)
    {
        DataTable ret = new DataTable();
        SqlConnection conn = null;
        SqlCommand cmd = null;
        try
        {
            conn = new SqlConnection(str_conn);
            cmd = new SqlCommand(comando, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 3600 * 2;
            if (nom_par != null && val_par != null)
                cmd.Parameters.AddWithValue(nom_par, val_par);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            conn.Open();
            da.Fill(ret);
            conn.Close();
            da.Dispose();
            cmd.Dispose();
            conn.Dispose();
        }
        catch (Exception e)
        {
            if (cmd != null)
                cmd.Dispose();
            if (conn != null)
            {
                conn.Close();
                conn.Dispose();
            }
        }
        return ret;
    }
    public static DataTable ejecutar_sp_dt(String comando, string str_conn) {
        DataTable ret = new DataTable();
        SqlConnection conn = null;
        SqlCommand cmd = null;
        try {
            conn = new SqlConnection(str_conn);
            cmd = new SqlCommand(comando, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 3600 * 2;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            conn.Open();
            da.Fill(ret);
            conn.Close();
            da.Dispose();
            cmd.Dispose();
            conn.Dispose();
        } catch (Exception e) {
            if (cmd != null)
                cmd.Dispose();
            if (conn != null) {
                conn.Close();
                conn.Dispose();
            }
        }
        return ret;
    }
    public static string ejecutar_sp_st(String comando, string[] nom_par, Object[] val_par, string str_conn)
    {
        string ret = "";
        SqlConnection conn = null;
        SqlCommand cmd = null;
        try
        {
            conn = new SqlConnection(str_conn);
            cmd = new SqlCommand(comando, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 3600 * 2;
            if (nom_par != null && val_par != null)
                for (int i = 0; i < Math.Min(nom_par.Length, val_par.Length); i++)
                    cmd.Parameters.AddWithValue(nom_par[i], val_par[i]);
            conn.Open();
            ret = cmd.ExecuteScalar().ToString();
            conn.Close();
            cmd.Dispose();
            conn.Dispose();
        }
        catch (Exception e)
        {
            if (cmd != null)
                cmd.Dispose();
            if (conn != null)
            {
                conn.Close();
                conn.Dispose();
            }
            ret = "Error :" + e.Message;
        }
        return ret;
    }
    public static string ejecutar_sp_st(String comando, String nom_par, Object val_par, string str_conn)
    {
        string ret = "";
        SqlConnection conn = null;
        SqlCommand cmd = null;
        try
        {
            conn = new SqlConnection(str_conn);
            cmd = new SqlCommand(comando, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 3600 * 2;
            cmd.CommandTimeout = 3600 * 2;
            if (nom_par != null && val_par != null)
                    cmd.Parameters.AddWithValue(nom_par, val_par);
            conn.Open();
            ret = cmd.ExecuteScalar().ToString();
            conn.Close();
            cmd.Dispose();
            conn.Dispose();
        }
        catch (Exception e)
        {
            if (cmd != null)
                cmd.Dispose();
            if (conn != null)
            {
                conn.Close();
                conn.Dispose();
            }
            ret = e.Message;
        }
        return ret;
    }
    public static DataSet ejecutar_sp_ds(String comando, string[] nom_par, Object[] val_par, string str_conn)
    {
        DataSet ret = new DataSet();
        SqlConnection conn = null;
        SqlCommand cmd = null;

        try
        {
            conn = new SqlConnection(str_conn);
            cmd = new SqlCommand(comando, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 3600 * 2;
            if (nom_par != null && val_par != null)
                for (int i = 0; i < Math.Min(nom_par.Length, val_par.Length); i++)
                    cmd.Parameters.AddWithValue(nom_par[i], val_par[i]);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            conn.Open();
            da.Fill(ret);
            conn.Close();
            cmd.Dispose();
            conn.Dispose();
        }
        catch (Exception e)
        {
            if (cmd != null)
                cmd.Dispose();
            if (conn != null)
            {
                conn.Close();
                conn.Dispose();
            }

        }
        return ret;
    }
    public static DataSet ejecutar_sp_ds(String comando, String nom_par, Object val_par, string str_conn)
    {
        DataSet ret = new DataSet();
        SqlConnection conn = null;
        SqlCommand cmd = null;
        try
        {
            conn = new SqlConnection(str_conn);
            cmd = new SqlCommand(comando, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 3600 * 2;
            if (nom_par != null && val_par != null)
                cmd.Parameters.AddWithValue(nom_par, val_par);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            conn.Open();
            da.Fill(ret);
            conn.Close();
            da.Dispose();
            cmd.Dispose();
            conn.Dispose();
        }
        catch (Exception e)
        {
            if (cmd != null)
                cmd.Dispose();
            if (conn != null)
            {
                conn.Close();
                conn.Dispose();
            }
        }
        return ret;
    }

    public static string[] ejecutar_sp_sa(String comando, String nom_par, Object val_par, string str_conn) {
        string [] ret = null;
        SqlConnection conn = null;
        SqlCommand cmd = null;
        try
        {
            conn = new SqlConnection(str_conn);
            cmd = new SqlCommand(comando, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 3600 * 2;
            if (nom_par != null && val_par != null)
                cmd.Parameters.AddWithValue(nom_par, val_par);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            
            ret = new string[dr.FieldCount];
            while (dr.Read())
            {
                for (int j = 0; j < dr.FieldCount; j++)
                {
                    ret[j] = dr[j].ToString();
                }
            }
            dr.Dispose();
            conn.Close();

            cmd.Dispose();
            conn.Dispose();
        }
        catch (Exception e)
        {
            if (cmd != null)
                cmd.Dispose();
            if (conn != null)
            {
                conn.Close();
                conn.Dispose();
            }
            ret = new string[] { e.Message };
        }
        return ret;
    }
    public static string cargar_archivo(string archivo, string tabla, string str_conn) {
        return cargar_archivo(archivo, tabla, null, null,str_conn);
    }
    public static string cargar_archivo(string archivo, string tabla, string config, string nombreHoja,string str_conn)
    {
        SqlConnection Connection = null;
        OleDbConnection myConnection = null;
        try
        {
            Connection = new SqlConnection(str_conn);
            
            String ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0; Data Source=" + archivo + "; Extended Properties='Excel 12.0;HDR=YES;IMEX=1;'";
            if (config == null || config == "") config = "*";
            if (nombreHoja == null || nombreHoja == "") nombreHoja = "[Hoja1$]";
            String CommandText = "SELECT "+config+" FROM "+nombreHoja;

            SqlCommand Command = new SqlCommand("Delete From " + tabla, Connection);
            Command.CommandType = CommandType.Text;
            Connection.Open();
            Command.ExecuteNonQuery();
            Connection.Close();
            Connection.Dispose();

            myConnection = new OleDbConnection(ConnectionString);
            OleDbCommand myCommand = new OleDbCommand(CommandText, myConnection);
            myConnection.Open();
            OleDbDataReader dr = myCommand.ExecuteReader();

            SqlBulkCopy bulkCopy = new SqlBulkCopy(str_conn);
            bulkCopy.DestinationTableName = tabla;
            bulkCopy.WriteToServer(dr);
            myConnection.Close();
            return OK;
        }
        catch (Exception e)
        {
            if (Connection != null && Connection.State == ConnectionState.Open)
            {
                Connection.Close();
                Connection.Dispose();
            }
            if (myConnection != null && myConnection.State == ConnectionState.Open)
            {
                myConnection.Close();
                myConnection.Dispose();
            }
            return "Error - "+e.Message;
        }
    }
    public static string cargar_archivo_txt(string archivo, string tabla, bool encabezados, char separador, string str_conn) {
        try {
            using (SqlConnection Connection = new SqlConnection(str_conn)) {
                using (SqlCommand Command = new SqlCommand("Delete From " + tabla, Connection)) {
                    Command.CommandType = CommandType.Text;
                    Connection.Open();
                    Command.ExecuteNonQuery();
                    Connection.Close();
                    Connection.Dispose();
                }
                int cols = 0;
                DataTable ret = new DataTable();
                using (StreamReader sr = new StreamReader(archivo)) {
                    string buffer = Regex.Replace(sr.ReadLine(), "[\"\t\r\n]", "");
                    if (buffer != null)
                        buffer = Regex.Replace(buffer, "[\"\t\r\n]", "");
                    string[] data;
                    if (buffer != null) {
                        if(buffer != null)
                            buffer = Regex.Replace(buffer, "[\"\t\r\n]", "");
                        data = buffer.Split(separador);
                        if (encabezados) {
                            for (int i = 0; i < data.Length; i++) {
                                ret.Columns.Add(data[i]);
                                cols++;
                            }
                            buffer = sr.ReadLine();
                            if (buffer != null)
                                buffer = Regex.Replace(buffer, "[\"\t\r\n]", "");
                        } else
                            for (int i = 0; i < data.Length; i++) {
                                ret.Columns.Add("" + i, typeof(string));
                                cols++;
                            }
                        while (buffer != null) {
                            data = buffer.Split(separador);
                            if (data.Length == cols)
                                ret.Rows.Add(data);
                            buffer = sr.ReadLine();
                            if (buffer != null)
                                buffer = Regex.Replace(buffer, "[\"\t\r\n]", "");
                        }
                    }
                }

                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(str_conn)) {
                    bulkCopy.BulkCopyTimeout = 2 * 3600;
                    bulkCopy.DestinationTableName = tabla;
                    bulkCopy.WriteToServer(ret);
                    bulkCopy.Close();
                }
                return "OK";
            }
        } catch (Exception ex) {
            return "Error - " + ex.Message;
        }
    }
    public static DataTable partartir_dt(DataTable dt, int cols)
    {
        if (dt == null || dt.Rows.Count == 0) return null;
        DataTable ret = new DataTable();
        for (int i = 0; i < cols; i++)
            ret.Columns.Add();
        int filas = (int)(dt.Columns.Count / cols) + ((dt.Columns.Count % cols == 0) ? 0 : 1);
        object[] vals;
        int min;
        for (int i = 0; i < filas; i++)
        {
            vals = new object[cols];
            min = ((i+1 ) * cols  > dt.Columns.Count ) ? dt.Columns.Count % cols : cols;
            for (int j = 0; j < min; j++)
                vals[j] = dt.Columns[i * cols + j].ColumnName;
            ret.Rows.Add(vals);
            vals = new object[cols];
            for (int j = 0; j < min; j++)
                vals[j] = dt.Rows[0][i * cols + j];
            ret.Rows.Add(vals);
        }

        return ret;
    }
    public static string formato_moneda(Double numero)
    {
        string num = ((int)Math.Round(numero, 0)) + "";
        string rta = "";
        int cmod = 0;
        for (int i = num.Length - 1; i >= 0; i--)
        {
            if (i != num.Length - 1 && cmod == 0)
                rta = "." + rta;
            rta = num[i] + rta;
            cmod = (cmod + 1) % 3;
        }
        return "$" + rta;
    }
    public static string formato_moneda(string num) {
        string rta = "";
        int cmod = 0;
        for (int i = num.Length - 1; i >= 0; i--) {
            if (i != num.Length - 1 && cmod == 0)
                rta = "." + rta;
            rta = num[i] + rta;
            cmod = (cmod + 1) % 3;
        }
        return "$" + rta;
    }

    public static string tabla_a_html(DataTable dt, string estilo) {
        StringBuilder sb = new StringBuilder();
        sb.Append("<table " + estilo + ">");
        for (int i = 0; i < dt.Columns.Count; i++)
            sb.Append("<th>" + dt.Columns[i].ColumnName + "</th>");

        for (int i = 0; i < dt.Rows.Count; i++) {
            sb.Append("<tr>");
            for (int j = 0; j < dt.Columns.Count; j++)
                sb.Append("<td>" + dt.Rows[i][j].ToString() + "</td>");
            sb.Append("<tr>");
        }
        sb.Append("</table>");
        return sb.ToString();
    }
    public static string tabla_a_json(DataTable dt) {
        StringBuilder sb = new StringBuilder();
        sb.Append("[");
        for (int i = 0; i < dt.Rows.Count; i++) {
            if (i > 0) sb.Append(",");
            sb.Append("{");
            for (int j = 0; j < dt.Columns.Count; j++) {
                if (j > 0) sb.Append(",");
                sb.Append("\"" + dt.Columns[j].ColumnName + "\":\"" + dt.Rows[i][j] + "\"");
            }
            sb.Append("}");
        }
        sb.Append("]");
        return sb.ToString();
    }
    public static string set_a_json(DataSet ds) {
        StringBuilder sb = new StringBuilder();

        DataTable dt;
        sb.Append("[");
        for (int k = 0; k < ds.Tables.Count; k++) {
            dt = ds.Tables[k];
            if (k > 0) sb.Append(",");
            sb.Append("[");
            for (int i = 0; i < dt.Rows.Count; i++) {
                if (i > 0) sb.Append(",");
                sb.Append("{");
                for (int j = 0; j < dt.Columns.Count; j++) {
                    if (j > 0) sb.Append(",");
                    sb.Append("\"" + dt.Columns[j].ColumnName + "\":\"" + dt.Rows[i][j] + "\"");
                }
                sb.Append("}");
            }
            sb.Append("]");
        }
        sb.Append("]");

        return sb.ToString();
    }
    public static string[] tabla_a_jsonz(DataTable dt)
    {
        string[] ret = new string[dt.Rows.Count];
        string elm;
        for (int i = 0; i < ret.Length; i++)
        {
            elm = "";
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                if (j != 0)
                    elm += ",";
                else elm += "{";
                elm += "\"" + dt.Columns[j].ColumnName + "\":\"" + dt.Rows[i][j] + "\"";
            }
            ret[i] = elm + "}";
        }
        return ret;
    }
    public static string tabla_a_json_v2(DataTable dt)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("[");
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (i > 0) sb.Append(",");
            sb.Append("{");
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                if (j > 0) sb.Append(",");
                sb.Append("\"" + dt.Columns[j].ColumnName + "\":\"" + dt.Rows[i][j] + "\"");
            }
            sb.Append("}");
        }
        sb.Append("]");
        return sb.ToString();
    }
    public static DataTable validar_parametros(string procedimiento, string str_conn) {
        DataTable ret = new DataTable();
        ret.Columns.Add("Parametro");
        ret.Columns.Add("Tipo");
        using (SqlConnection conn = new SqlConnection(str_conn)) {
            using (SqlCommand cmd = new SqlCommand(procedimiento, conn)) {
                cmd.CommandType = CommandType.StoredProcedure;
                conn.Open();
                SqlCommandBuilder.DeriveParameters(cmd);
                foreach (SqlParameter p in cmd.Parameters)
                    ret.Rows.Add(p.ParameterName, p.SqlDbType.ToString());
            }
        }
        return ret;
    }
    public static DataTable obtener_columnas(string tabla, string str_conn) {
        DataTable ret = new DataTable();
        using (SqlConnection Connection = new SqlConnection(str_conn)) {
            using (SqlCommand Command = new SqlCommand("select top 0 * from " + tabla, Connection)) { 
                Command.CommandType = CommandType.Text;
                SqlDataAdapter da = new SqlDataAdapter(Command);
                Connection.Open();
                da.Fill(ret);
                ret.Rows.Add(ret.NewRow());
                Connection.Close();
                da.Dispose();
            }
        }
        return ret;
    }
    public static DataTable excel_a_tabla_vp(string path, int registros, out string nombreHoja, bool encabezados, char separador) {
        DataTable ret = new DataTable();
        if (Path.GetExtension(path).ToLower() == ".xlsx") {
            using (XLWorkbook wb = new XLWorkbook(path)) {
                IXLWorksheet ws = wb.Worksheet(1);
                nombreHoja = ws.Name;
                IXLRow row = ws.FirstRowUsed();
                if (row != null) {
                    IXLCells cells = row.CellsUsed();
                    if (cells != null) {
                        string col;
                        HashSet<string> hs = new HashSet<string>();
                        int icol = -1, fcol = -1;
                        foreach (IXLCell cell in cells) {
                            if (icol == -1) icol = cell.Address.ColumnNumber;
                            fcol = cell.Address.ColumnNumber;
                            col = Regex.Replace(cell.Value.ToString(), "[\"\t\r\n]", "");
                            while (hs.Contains(col))
                                col += "_I";
                            ret.Columns.Add(col);
                            hs.Add(col);
                        }

                        int cols = ret.Columns.Count;
                        int rCols = 0;
                        row = row.RowBelow();
                        DataRow dr;
                        bool stop;
                        while (row != null && registros >= 0) {
                            dr = ret.NewRow();
                            cells = row.Cells(icol, fcol);
                            rCols = 0;
                            stop = true;
                            foreach (IXLCell cell in cells) {
                                if (rCols >= cols) break;
                                if (cell.ValueCached != null)
                                    col = Regex.Replace(cell.ValueCached, "[\"\t\r\n]", "");
                                else col = Regex.Replace(cell.Value.ToString(), "[\"\t\r\n]", "");
                                dr[rCols] = col;
                                if (col != "") stop = false;
                                rCols++;
                            }
                            if (stop) break;
                            ret.Rows.Add(dr);
                            row = row.RowBelow();
                            registros--;
                        }
                    }
                }
            }
        } else {
            nombreHoja = "";
            int cols = 0;
            using (StreamReader sr = new StreamReader(path)) {
                string buffer = Regex.Replace(sr.ReadLine(), "[\"\t\r\n]", "");
                string[] data;
                if (buffer != null) {
                    data = buffer.Split(separador);
                    if (encabezados) {
                        for (int i = 0; i < data.Length; i++) {
                            ret.Columns.Add(data[i]);
                            cols++;
                        }
                        buffer = Regex.Replace(sr.ReadLine(), "[\"\t\r\n]", "");
                    } else
                        for (int i = 0; i < data.Length; i++) {
                            ret.Columns.Add("" + i, typeof(string));
                            cols++;
                        }
                    while (buffer != null && registros >= 0) {
                        data = buffer.Split(separador);
                        if (data.Length == cols)
                            ret.Rows.Add(data);
                        buffer = Regex.Replace(sr.ReadLine(), "[\"\t\r\n]", "");
                        registros--;
                    }
                } 
            }
        }
        return ret;
    }
}