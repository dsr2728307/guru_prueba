﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;

/// <summary>
/// Descripción breve de Tipificacion
/// </summary>
public class Tipificacion
{
    public int id,nivel, padre;
    public string valor;
    public List<Tipificacion> hijos;
    public Tipificacion() {
        hijos = new List<Tipificacion>();
    }
    

    public static Tipificacion[] construir(DataTable dt){
        Dictionary<int, Tipificacion> tip = new Dictionary<int,Tipificacion>();
        List<Tipificacion> ret = new List<Tipificacion>();
        Tipificacion tmp;
        Queue<int> pendientes = new Queue<int>();
        string tvalor;
        int tid, tpadre;
        for (int i = 0; i < dt.Rows.Count; i++) {
            tid = (int)dt.Rows[i][0];
            tvalor = (string)dt.Rows[i][1];
            tpadre = (int)dt.Rows[i][2];
            tmp = new Tipificacion { id = tid, valor = tvalor, padre = tpadre };
            tip[tid] = tmp;
            if (tpadre < 0)
                ret.Add(tmp);
            else 
                if (tip.ContainsKey(tpadre)) 
                    tip[tpadre].hijos.Add(tmp);
                else 
                    pendientes.Enqueue(tid);
        }
        bool cont = true;
        while (pendientes.Count > 0 && cont) {
            cont = false;
            tmp = tip[pendientes.Dequeue()];
            if (tip.ContainsKey(tmp.padre)) {
                tip[tmp.padre].hijos.Add(tmp);
                cont = true;
            } else
                pendientes.Enqueue(tmp.id);
        }
        //return ret.ToArray<Tipificacion>(); // .NET 3.5
        return ret.ToArray(); // .NET 4.0
    }
}