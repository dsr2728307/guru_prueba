﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Descripción breve de BDETB
/// </summary>
public class BDUdn
{

    private static string str_conn = ConfigurationManager.ConnectionStrings["Base"].ConnectionString;
    public static DataTable ejecutar_sp_dt(String comando, String[] nom_par, Object[] val_par)
    {
        return BDUt.ejecutar_sp_dt(comando, nom_par, val_par, str_conn);
    }
    public static DataTable ejecutar_sp_dt(String comando) {
        return BDUt.ejecutar_sp_dt(comando, str_conn);
    }
    public static DataTable ejecutar_sp_dt(String comando, String nom_par, Object val_par)
    {
        return BDUt.ejecutar_sp_dt(comando, nom_par, val_par, str_conn);
    }
    public static string[] ejecutar_sp_sa(String comando, String nom_par, Object val_par)
    {
        return BDUt.ejecutar_sp_sa(comando, nom_par, val_par, str_conn);
    }
    public static string ejecutar_sp_st(String comando, String nom_par, Object val_par)
    {
        return BDUt.ejecutar_sp_st(comando, nom_par, val_par, str_conn);
    }
    public static string ejecutar_sp_st(String comando, String[] nom_par, Object[] val_par)
    {
        return BDUt.ejecutar_sp_st(comando, nom_par, val_par, str_conn);
    }
    public static DataSet ejecutar_sp_ds(String comando, String nom_par, Object val_par)
    {
        return BDUt.ejecutar_sp_ds(comando, nom_par, val_par, str_conn);
    }
    public static string cargar_archivo(string archivo, string tabla, string configuracion, string nombreHoja)
    {
        return BDUt.cargar_archivo(archivo, tabla, configuracion, nombreHoja,str_conn);
    }
    public static string cargar_archivo_txt(string archivo, string tabla, bool encabezados, char separador) {
        return BDUt.cargar_archivo_txt(archivo, tabla, encabezados, separador, str_conn);
    }
    public static DataTable validar_parametros(string procedimiento) {
        return BDUt.validar_parametros(procedimiento, str_conn);
    }

    public static DataSet ejecutar_sp_ds(String comando, string[] nom_par, Object[] val_par){
        return BDUt.ejecutar_sp_ds(comando, nom_par, val_par, str_conn);
    }
    public static DataTable obtener_columnas(string tabla) {
        return BDUt.obtener_columnas(tabla, str_conn);
    }
}