﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e){
        //Roles.CreateRole("prueba");
        //Roles.AddUserToRole("administrador", "prueba");
            if(!User.Identity.IsAuthenticated)
                Response.Redirect("./Account/Login.aspx");
        }
    }
