﻿<%@ Page Title="Change Password" Language="C#" MasterPageFile="~/Maestra.master"
    AutoEventWireup="true" CodeFile="ChangePassword.aspx.cs" Inherits="ChangePassword" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
    <style type="text/css">
        .log
        {
            border:1px solid #d9d9d9;
            border-radius: 3px;
            width: 50%;
            margin: 0px auto;
        }
        .log > div
        {
            background-color:#d9d9d9;
            padding: 5px 20px;
            margin-bottom: 40px;
            text-align:left;
        }
        
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <h2>
        Cambio de Contraseña
    </h2>
    <p>
        La nueva contraseña debe tener al menos
        <%= Membership.MinRequiredPasswordLength %>
        caracteres.
    </p>
    <center>
        <asp:ChangePassword ID="ChangeUserPassword" runat="server" CancelDestinationPageUrl="~/"
            EnableViewState="false" RenderOuterTable="false" SuccessPageUrl="ChangePasswordSuccess.aspx"
            ChangePasswordFailureText="La contraseña no es correcta">
            <ChangePasswordTemplate>
                <span class="failureNotification">
                    <asp:Literal ID="FailureText" runat="server"></asp:Literal>
                </span>
                <asp:ValidationSummary ID="ChangeUserPasswordValidationSummary" runat="server" CssClass="failureNotification"
                    ValidationGroup="ChangeUserPasswordValidationGroup" />
                <div style="width:100%; min-height: 12vh;"></div>
                
                    <div class="log">
                        <div>Información del la Cuenta</div>
                        <table style="width: 80%; ">
                            <tr>
                                <td>
                                    <asp:Label ID="CurrentPasswordLabel" runat="server" AssociatedControlID="CurrentPassword">Anterior Contraseña</asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="CurrentPassword" runat="server" CssClass="passwordEntry" TextMode="Password"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="CurrentPasswordRequired" runat="server" ControlToValidate="CurrentPassword"
                                        CssClass="failureNotification" ErrorMessage="Es necesaria la anterior contraseña."
                                        ToolTip="Es necesaria la anterior ." ValidationGroup="ChangeUserPasswordValidationGroup">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="NewPasswordLabel" runat="server" AssociatedControlID="NewPassword">Nueva Contraseña</asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="NewPassword" runat="server" CssClass="passwordEntry" TextMode="Password"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="NewPasswordRequired" runat="server" ControlToValidate="NewPassword"
                                        CssClass="failureNotification" ErrorMessage="Es necesaria una nueva contraseña."
                                        ToolTip="Es necesaria una nueva contraseña." ValidationGroup="ChangeUserPasswordValidationGroup">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="ConfirmNewPasswordLabel" runat="server" AssociatedControlID="ConfirmNewPassword">Confirmar Nueva Contraseña</asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="ConfirmNewPassword" runat="server" CssClass="passwordEntry" TextMode="Password"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" ControlToValidate="ConfirmNewPassword"
                                        CssClass="failureNotification" Display="Dynamic" ErrorMessage="Falta confirmar la nueva Contraseña."
                                        ToolTip="Confirm New Password is required." ValidationGroup="ChangeUserPasswordValidationGroup">*</asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="NewPasswordCompare" runat="server" ControlToCompare="NewPassword"
                                        ControlToValidate="ConfirmNewPassword" CssClass="failureNotification" Display="Dynamic"
                                        ErrorMessage="Las contraseñas no coinciden." ValidationGroup="ChangeUserPasswordValidationGroup">*</asp:CompareValidator>
                                </td>
                            </tr>
                            <tr>
                                <td><br /></td>
                            </tr>
                        </table>
                        <p class="submitButton">
                            <asp:Button ID="ChangePasswordPushButton" runat="server" CommandName="ChangePassword"
                                Text="Cambiar Contraseña" ValidationGroup="ChangeUserPasswordValidationGroup" />
                            <asp:Button ID="CancelPushButton" runat="server" CausesValidation="False" CommandName="Cancel"
                                Text="Cancelar" />
                        </p>
                    </div>
            </ChangePasswordTemplate>
        </asp:ChangePassword>
    </center>
</asp:Content>
