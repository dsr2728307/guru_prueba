﻿<%@ Page Title="Acceder" Language="C#" MasterPageFile="~/Maestra.master" AutoEventWireup="true"
    CodeFile="Login.aspx.cs" Inherits="Login" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
<style type="text/css">
        .log
        {
            border:1px solid #d9d9d9;
            border-radius: 3px;
            width: 50%;
            margin: 0px auto;
        }
        .log > div
        {
            background-color:#d9d9d9;
            padding: 5px 20px;
            margin-bottom: 40px;
            text-align:left;
        }
        
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <h2>
        Ingreso al Sistema
    </h2>
    <p>
        Por favor ingrese su usuario y contraseña.
    </p>
    <center>
        <asp:Login ID="LoginUser" runat="server" EnableViewState="false" RenderOuterTable="false"
            FailureText="Usuario o conraseña inválidos" style="width:100%">
            <LayoutTemplate>
                <span class="failureNotification">
                    <asp:Literal ID="FailureText" runat="server"></asp:Literal>
                </span>
                <asp:ValidationSummary ID="LoginUserValidationSummary" runat="server" CssClass="failureNotification"
                    ValidationGroup="LoginUserValidationGroup" />
                <div style="width:100%; min-height: 11vh;"></div>
                    <div class="log">
                        <div>Información del la Cuenta</div>
                        <table style="margin: 0px auto;">
                            <tr>
                                <td>
                                    <img src ="../CSS/img/userx24.png" alt="" />
                                </td>
                                <td>
                                    <asp:TextBox ID="UserName" runat="server" Width="200px" placeholder="Usuario"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                                        CssClass="failureNotification" ErrorMessage="Debe ingresar un nombre de usuario."
                                        ToolTip="Debe ingresar un nombre de usuario." ValidationGroup="LoginUserValidationGroup">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img src ="../CSS/img/keyx24.png" alt="" />
                                </td>
                                <td>
                                    <asp:TextBox ID="Password" runat="server" TextMode="Password" Width="200px" placeholder="Contraseña"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                                        CssClass="failureNotification" ErrorMessage="Debe ingresar su contraseña." ToolTip="Debe ingresar su contraseña."
                                        ValidationGroup="LoginUserValidationGroup">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center">
                                    <br />
                                    <br />
                                    <asp:Button ID="LoginButton" runat="server" CommandName="Login" Text="Acceder" ValidationGroup="LoginUserValidationGroup" />
                                </td>
                            </tr>
                        </table>
                        <br />
                        <br />
                    </div>
            </LayoutTemplate>
        </asp:Login>
    </center>
</asp:Content>
