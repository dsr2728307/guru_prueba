﻿using System;
using System.Web.Security;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.UI;

    public partial class Register : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e){
            if (!IsPostBack)
                actualizar_datos();
        }
        private void actualizar_datos() {
            DataTable dt = new DataTable();
            dt.Columns.Add("Usuario");
            dt.Columns.Add("Rol");
            dt.Columns.Add("Bloqueado");
            foreach (MembershipUser user in Membership.GetAllUsers()){
                if (user.UserName != "administrador")
                    dt.Rows.Add(user.UserName, string.Join(", ",Roles.GetRolesForUser(user.UserName)), (user.IsLockedOut ? "Si" : "No"));
            }
            gv_usuario.DataSource = dt;
            gv_usuario.DataBind();

            dt = new DataTable();
            dt.Columns.Add("ID"); dt.Columns.Add("VALUE");
            int id = 0;
            foreach (string rol in Roles.GetAllRoles())
                if (rol != "admin")
                dt.Rows.Add(id++, rol);
            dd_roles.DataSource = dt;
            dd_roles.DataBind();
        }
        protected void crear_usuario(object sender, EventArgs e){
            string name = UserName.Text;
            MembershipUser user = Membership.GetUser(name);
            if (user != null) {
                mostrar_mensaje("Ya exixte el usuario: " + name);
                return;
            }
            if (Password.Text.Length < Membership.MinRequiredPasswordLength) {
                mostrar_mensaje("La contraseña debe tener al menos " + Membership.MinRequiredPasswordLength + " caracteres");
                return;
            }
            user = Membership.CreateUser(name, Password.Text);
            Roles.AddUserToRole(user.UserName, dd_roles.SelectedItem.Text);
            mostrar_mensaje("Se creó el usuario: " + name);
            actualizar_datos();

        }
        protected void editar_usuario(object sender, CommandEventArgs e){
            string id = ((GridView)sender).Rows[Int32.Parse(e.CommandArgument.ToString())].Cells[2].Text;
            string msg="";
            if (e.CommandName == "ELIMINAR"){
                Membership.DeleteUser(id,true);
                msg = "Se eliminó el usuario: " + id;
            }
            else {
                MembershipUser user = Membership.GetUser(id);
                if (user.IsLockedOut)
                    user.UnlockUser();
                string pass = user.ResetPassword();
                msg = "La nueva contraseña de usuario es: " + pass;
            }
            mostrar_mensaje(msg);
            actualizar_datos();
        }
        private void mostrar_mensaje(string mensaje){
            string cleanMessage = mensaje.Replace("'", "").Replace("\\", "").Replace("\"", "").Replace("\r\n", "");
            string script = string.Format("mostrar_mensaje('{0}');", cleanMessage);
            ScriptManager.RegisterStartupScript(this, GetType(), "alerta", script, true);
        }

    }
