﻿<%@ Page Title="Administración de Usuarios" Language="C#" MasterPageFile="~/Maestra.master"
    AutoEventWireup="true" CodeFile="Register.aspx.cs" Inherits="Register" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
     <style type="text/css">
        .log
        {
            border:1px solid #d9d9d9;
            border-radius: 3px;
            width: 50%;
            margin: 0px auto;
        }
        .log > div
        {
            background-color:#d9d9d9;
            padding: 5px 20px;
            margin-bottom: 40px;
            text-align:left;
        }
        
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <asp:ScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="3600"
        EnablePageMethods="true" />
    <table style="margin: 0px auto; width: 90%;">
        <tr>
            <td>
                <h2>
                    Crear un nuevo Usuario
                </h2>
                <p>
                    la contraseña debe tener al menos
                    <%= Membership.MinRequiredPasswordLength %>
                    caracteres.
                </p>
                <span class="failureNotification">
                    <asp:Literal ID="ErrorMessage" runat="server"></asp:Literal>
                </span>
                <asp:ValidationSummary ID="RegisterUserValidationSummary" runat="server" CssClass="failureNotification"
                    ValidationGroup="RegisterUserValidationGroup" />
                <div class="log" style="width:60%">
                        <div>Información del la Cuenta</div>
                        <table style="width: 80%; margin:0px auto">
                            <tr>
                                <td>
                                    <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">Usuario</asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="UserName" runat="server" MaxLength="20" ></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                                        CssClass="failureNotification" ErrorMessage="Debe introducir un nombre de usuario."
                                        ToolTip="Debe introducir un nombre de usuario." ValidationGroup="RegisterUserValidationGroup">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">Contraseña</asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="Password" runat="server" TextMode="Password" ></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                                        CssClass="failureNotification" ErrorMessage="Debe introducir una contraseña."
                                        ToolTip="Debe introducir una contraseña." ValidationGroup="RegisterUserValidationGroup">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="ConfirmPasswordLabel" runat="server" AssociatedControlID="ConfirmPassword">Confirmar Contraseña</asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="ConfirmPassword" runat="server" TextMode="Password" ></asp:TextBox>
                                    <asp:RequiredFieldValidator ControlToValidate="ConfirmPassword" CssClass="failureNotification"
                                        Display="Dynamic" ErrorMessage="Debe confirmar la contraseña." ID="ConfirmPasswordRequired"
                                        runat="server" ToolTip="Confirm Password is required." ValidationGroup="RegisterUserValidationGroup">*</asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="PasswordCompare" runat="server" ControlToCompare="Password"
                                        ControlToValidate="ConfirmPassword" CssClass="failureNotification" Display="Dynamic"
                                        ErrorMessage="La contraseña y su confirmación no coinciden." ValidationGroup="RegisterUserValidationGroup">*</asp:CompareValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label1" runat="server">Rol</asp:Label><br />
                                </td>
                                <td>
                                    <asp:DropDownList runat="server" ID="dd_roles" Width="100%" DataValueField="ID" DataTextField="VALUE" style="margin-top: 10px;"/>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center">
                                <br /><br />
                                    <asp:Button ID="CreateUserButton" runat="server" CommandName="MoveNext" Text="Crear Usuario"
                                        ValidationGroup="RegisterUserValidationGroup" OnClick="crear_usuario" />
                                </td>
                            </tr>
                        </table>
                        <br /><br /><br />
                </div>
            </td>
           <td style="vertical-align: top;">
                <h2>
                    Usuarios Existentes
                </h2>
                <br />
                <div style="overflow: auto; height: 400px;">
                    <asp:GridView ID="gv_usuario" runat="server" CssClass="gv_normal" OnRowCommand="editar_usuario">
                        <Columns>
                            <asp:ButtonField Text="Eliminar" ButtonType="Button" CommandName="ELIMINAR" ControlStyle-CssClass="bt_rojo" />
                            <asp:ButtonField Text="Reiniciar" ButtonType="Button" CommandName="REINICIAR" ControlStyle-CssClass="bt_verde" />
                        </Columns>
                    </asp:GridView>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
