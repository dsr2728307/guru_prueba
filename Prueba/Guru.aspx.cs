﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Prueba_Guru : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [WebMethod]
    public static string getproductos()
    {
        return BDUt.tabla_a_json(BDUdn.ejecutar_sp_dt("Wp_Get_Producto"));
    }
    [WebMethod]
    public static string insProducto(int Id,string Codigo_Producto, string Producto, string Descripcion, decimal Precio, decimal Cantidad)
    {
        string[] pars = { "@Id", "@Codigo_Producto", "@Producto", "@Descripcion", "@Precio", "@Cantidad","@Total" };
        object[] vals = { Id, Codigo_Producto, Producto, Descripcion, Precio, Convert.ToInt32(Cantidad) , Precio* Convert.ToInt32(Cantidad) };
        return BDUdn.ejecutar_sp_st("Wp_Ins_Producto", pars, vals);
    }
    [WebMethod]
    public static string DelProducto(int Id)
    {
        string[] pars = { "@Id" };
        object[] vals = { Id };
        return BDUdn.ejecutar_sp_st("Wp_Del_Producto", pars, vals);
    }
}