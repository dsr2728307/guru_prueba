﻿<%@ Page Title="Prueba Guru"  Language="C#" MasterPageFile="~/Maestra.master" AutoEventWireup="true" CodeFile="Guru.aspx.cs" Inherits="Prueba_Guru" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="../Scripts/Ut.js" type="text/javascript"></script>
    <script src="../Scripts/tabs/HomeMadeTabs.js" type="text/javascript"></script>
    <link href="../Scripts/tabs/HomeMadeStyle.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function prevenir_enter(e) {
            var key = e.charCode || e.keyCode || 0;
            if (key == 13) {
                e.preventDefault();
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <ajax:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="3600"
        EnablePageMethods="true" />
    <div data-ng-app="GuruApp">
        <!--------------------------------------------------------------------------------- CLIENTE ------------------------------------------------------->
        <div style="margin-top:5px;" data-ng-controller="GuruCtrl" data-ng-init="init();">
            <div class="button" data-ng-click="proceso ='1'"  tabindex="0">Insertar</div>
            <br />
            <br />
            <table style="width: 100%" class="gv_normal">
                <tr>
                    <th></th>
                    <th></th>
                    <th>Código producto</th>
                    <th>Producto</th>
                    <th>Descripción</th>
                    <th>Precio</th>
                    <th>Cantidad</th>
                    <th>Total</th>
                    <th>Fecha de Ingreso</th>
                    <th style="width:70px" data-ng-show="proceso !='0'"></th>
                    <th style="width:70px" data-ng-show="proceso !='0'"></th>
                </tr>
                <tr data-ng-show="proceso !='0'" class="mini_input">
                    <td></td>
                    <td></td>
                    <td><input type="text" data-ng-model="parsProducto.Codigo_Producto" placeholder="Código producto" maxlength="1000" style="text-align:center"/></td>
                    <td><input type="text" data-ng-model="parsProducto.Producto" placeholder="Producto" maxlength="1000" style="text-align:center"/></td>
                    <td><input type="text" data-ng-model="parsProducto.Descripcion" placeholder="Descripción"  style="text-align:center"/></td>
                    <td><input type="number" data-ng-model="parsProducto.Precio" placeholder="Precio"  style="text-align:center"/></td>
                    <td><input type="number" data-ng-model="parsProducto.Cantidad" placeholder="Cantidad" maxlength="1000" style="text-align:center"/></td>
                    <td>{{parsProducto.Precio*parsProducto.Cantidad| currency}}</td>
                    <td></td>
                    <td><div class="mini_button" data-ng-class="proceso =='1'? 'mini_button_solid_green': 'mini_button_solid_yellow'" data-ng-click="guardar();">{{proceso=='1'?'Insertar':'Actualizar'}}</div></td>
                    <td><div class="mini_button" data-ng-class="'mini_button_solid_red'" data-ng-click="resetParsGuru();">Cancelar</div></td>
                </tr>
                <tr data-ng-repeat="prod in productos">
                    <td><div class="mini_button" data-ng-click="DelProducto(prod.Id);"><img src="../CSS/ico/ic_rmv.png" /></div></td>
                    <td><div class="mini_button" data-ng-click="EditProducto(prod);"><img src="../CSS/ico/edit.png" /></div></td>
                    <td data-ng-bind="prod.Codigo_Producto"></td>
                    <td data-ng-bind="prod.Producto"></td>
                    <td data-ng-bind="prod.Descripcion"></td>
                    <td data-ng-bind="prod.Precio| currency"></td>
                    <td data-ng-bind="prod.Cantidad| currency"></td>
                    <td data-ng-bind="prod.Total| currency"></td>
                    <td data-ng-bind="prod.Fecha_Ingreso"></td>
                </tr>
            </table>

        </div>
    </div>
    <br />
    <script src="../Scripts/angular/angular.min.js" type="text/javascript"></script>
    <script src="../Scripts/angular/angular-sanitize.js" type="text/javascript"></script>
    <script src="../Scripts/angular/angular-http-helper.js" type="text/javascript"></script>
    <script type="text/javascript">
        var app = angular.module('GuruApp', ['ngSanitize']);
    </script>
    <script src="Guru.js" type="text/javascript"></script>
</asp:Content>