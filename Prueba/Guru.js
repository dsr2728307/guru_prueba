﻿app.controller('GuruCtrl', function ($scope, $http, $rootScope) {
    $scope.proceso = 0; /*0- sin proceso, 1 - insertar , 2 - actualizar*/
    $scope.parsProducto = {
        Id : "0",
        Codigo_Producto : "",
        Producto : "",
        Descripcion : "",
        Precio : 0,
        Cantidad : 1,
        Total : 0,
        Fecha_Ingreso : null

    };
    $scope.productos = [];
    $scope.resetParsGuru = function () {
        $scope.parsProducto = {
            Id: "0",
            Codigo_Producto: "",
            Producto: "",
            Descripcion: "",
            Precio: 0,
            Cantidad: 1

        };

        $scope.proceso = 0;
    };
    $scope.init = function () {

        $scope.getproductos();
    };

    $scope.getproductos = function () {
        httpCallObj($http, 'Guru.aspx/getproductos', {}, function (data) {
            $scope.productos = data;
        });
    };
    $scope.guardar = function () {
        var msg = "";
        if ($scope.parsProducto.Codigo_Producto == "" || $scope.parsProducto.Codigo_Producto == null) msg = "Error - El valor del campo Codigo Producto es inválido "
        else if ($scope.parsProducto.Producto == "" || $scope.parsProducto.Producto == null) msg = "Error - El valor del campo Producto es inválido "
        else if ($scope.parsProducto.Precio == "" || $scope.parsProducto.Precio == "0" || $scope.parsProducto.Precio == null) msg = "Error - El valor del campo Precio es inválido "
        else if ($scope.parsProducto.Cantidad == "" || $scope.parsProducto.Cantidad == "0" || $scope.parsProducto.Cantidad == null) msg = "Error - El valor del campo Cantidad es inválido "
        
        if (msg != "") { mostrar_mensaje(msg); return; }
        mostrar_progreso();

        httpCallStr($http, 'Guru.aspx/insProducto', $scope.parsProducto, function (data) {
            mostrar_mensaje(data);
            ocultar_progreso();
            if (data.indexOf("Error") < 0) {
                $scope.resetParsGuru();
                $scope.getproductos();
            }
        });
    };
    $scope.DelProducto = function (Id) {
        mostrar_progreso();
        httpCallStr($http, 'Guru.aspx/DelProducto', { Id: Id }, function (data) {
            ocultar_progreso();
            mostrar_mensaje(data);
            if (data.indexOf("Error") < 0) {
                $scope.getproductos();
            }
        });
    };
    $scope.EditProducto = function (prod) {

        $scope.parsProducto.Id = prod.Id ;
        $scope.parsProducto.Codigo_Producto = prod.Codigo_Producto ;
        $scope.parsProducto.Producto = prod.Producto ;
        $scope.parsProducto.Descripcion = prod.Descripcion ;
        $scope.parsProducto.Precio = parseFloat(prod.Precio) ;
        $scope.parsProducto.Cantidad = parseFloat(prod.Cantidad) ;

        $scope.proceso = 2;
    };
});