﻿<%@ Page Title="Inicio" Language="C#" MasterPageFile="~/Maestra.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="Default" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
    <link href="CSS/Varios.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .amenu
        {
            display: inline-block;
            width: 480px;
        }
        .bmenu
        {
            border: 1px solid #d9d9d9;
            display: inline-block;
            width: 230px;
            height: 100px;
            border-radius: 3px;
            margin: 5px 1px;
            cursor: pointer;
        }
        .bmenu:hover
        {
            background-color: #e6f7ff;
        }
        .bmenu > table
        {
            width: 100%;
            border-collapse: collapse;
        }
        .bmenu > table th
        {
            background-color: #d9d9d9;
            padding: 5px 20px;
            text-align: left;
        }
        .bmenu > table td
        {
            padding: 5px 10px;
            text-align: justify;
        }
        .bmenu > table img
        {
            width: 40px;
        }
        h2, h3
        {
            margin-top: 0px;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <h2>
        Menú Principal
    </h2>
    <br />
    <br />
    <br />
    <asp:LoginView ID="LoginView1" runat="server" EnableViewState="false">
        <AnonymousTemplate>
            <br />
        </AnonymousTemplate>
        <RoleGroups>
            <asp:RoleGroup Roles="prueba">
                <ContentTemplate>
                    <div style="text-align: center">
                        <div class="bmenu" onclick="window.location.href='Prueba/Guru.aspx'">
                            <table>
                                <tr>
                                    <th colspan="2">
                                        Prueba Solicitada
                                    </th>
                                </tr>
                                <tr>
                                    <td style="width: 50px">
                                        <img src="CSS/img/workerx64.png" />
                                    </td>
                                    <td style="width: 100%">
                                        Pagina encargada de requerimientos.
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="bmenu" onclick="window.location.href='Account/ChangePassword.aspx'">
                            <table>
                                <tr>
                                    <th colspan="2">
                                        Administración de Cuenta
                                    </th>
                                </tr>
                                <tr>
                                    <td style="width: 50px">
                                        <img src="CSS/img/lockx34.png" />
                                    </td>
                                    <td style="width: 100%">
                                        Cambio y renovación de contraseñas.
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="bmenu" onclick="window.location.href='Account/Register.aspx'">
                            <table>
                                <tr>
                                    <th colspan="2">
                                        Administración de Usuarios
                                    </th>
                                </tr>
                                <tr>
                                    <td style="width: 50px">
                                        <img src="CSS/img/usuariosx64.png" />
                                    </td>
                                    <td style="width: 100%">
                                        Permite crear usuarios, eliminarlos, cambiar su perfil y reestablecer la contraseña.
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:RoleGroup>
        </RoleGroups>
    </asp:LoginView>
</asp:Content>
