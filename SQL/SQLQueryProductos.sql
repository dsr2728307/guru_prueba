Create table Productos(
Id bigint primary key identity(1,1),
Codigo_Producto varchar(1000),
Producto varchar(1000),
Descripcion varchar(max),
Precio decimal(18,2),
Cantidad int,
Total decimal(18,2),
Fecha_Ingreso datetime default (getdate())
)
go
-- =============================================
/*
Author:	Santiago Reyes 
Create date: 21-10-2021 
Description: procedimiento encargado de extraer los productos
*/
-- =============================================

Create Procedure  Wp_Get_Producto
AS 
Begin
set transaction isolation level read uncommitted
Begin Try  

/*proceso a ejecutar*/
select
Id,
Codigo_Producto,
Producto,
Descripcion,
Precio,
Cantidad,
Total,
convert(varchar,Fecha_Ingreso,103) Fecha_Ingreso
from Productos

End Try          
Begin catch       
  SELECT 'Error - ' + error_message()           
End Catch  
End
go
-- =============================================
/*
Author:	Santiago Reyes 
Create date: 21-10-2021 
Description: procedimiento encargado de eliminar los productos
*/
-- =============================================

Create Procedure  Wp_Del_Producto
@Id int
AS 
Begin
set transaction isolation level read uncommitted
Begin Try  

/*proceso a ejecutar*/
delete  Productos
where Id = @Id
select 'Se ha eliminado correctamente'

End Try          
Begin catch       
  SELECT 'Error - ' + error_message()           
End Catch  
End
go
-- =============================================
/*
Author:	Santiago Reyes 
Create date: 21-10-2021 
Description: procedimiento encargado de Insertar los productos
*/
-- =============================================

alter Procedure  Wp_Ins_Producto
@Id int,
@Codigo_Producto varchar(1000),
@Producto varchar(1000),
@Descripcion varchar(max),
@Precio decimal(18,2),
@Cantidad int,
@Total decimal(18,2)
AS 
Begin
set transaction isolation level read uncommitted
Begin Try  

/*proceso a ejecutar*/

if(@Id = 0)
Begin
	insert into Productos(Codigo_Producto,Producto,Descripcion,Precio,Cantidad,Total)
	select @Codigo_Producto,@Producto,@Descripcion,@Precio,@Cantidad,@Total

	select 'Se ha Insertado correctamente'
end
else 
begin
	update Productos
	set Codigo_Producto = @Codigo_Producto,
		Producto = @Producto,
		Descripcion = @Descripcion,
		Precio = @Precio,
		Cantidad = @Cantidad,
		Total =@Total,
		Fecha_Ingreso = GETDATE()
	where Id = @Id
	select 'Se ha actualizado correctamente'
end


End Try          
Begin catch       
  SELECT 'Error - ' + error_message()           
End Catch  
End
go
