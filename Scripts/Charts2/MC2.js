﻿var dict = {};
var vdict = {};

var options_bar = {
    responsive: true
};
var options_pie = {
    responsive: true
};
var options_line = {
    responsive: true
};
function display_json_bar(sdata, canvas_id) {
    if (dict[canvas_id] != null) {
        dict[canvas_id].clear();
        dict[canvas_id].destroy();
    }
    var ctx = document.getElementById(canvas_id).getContext("2d");
    var data;
    try {
        data = JSON.parse(sdata);
    } catch (err) {
        console.log(err.message);
        return;
    }
    var config = {
        type: 'bar',
        data: data,
        options: options_bar
    };
    var myChart = new Chart(ctx, config);
    return myChart;
}
function display_json_pie(sdata, canvas_id) {
    if( dict[canvas_id] != null){
        dict[canvas_id].clear();
        dict[canvas_id].destroy();
    }
    var ctx = document.getElementById(canvas_id).getContext("2d");
    var data;
    try {
        data = JSON.parse(sdata);
    } catch (err) {
        console.log(err.message);
        return;
    }
    var config = {
        type: 'pie',
        data: data,
        options: options_pie
    };
    var myChart = new Chart(ctx, config);
    return myChart;
}
function display_json_line(sdata, canvas_id) {
    if (dict[canvas_id] != null) {
        dict[canvas_id].clear();
        dict[canvas_id].destroy();
    }
    var ctx = document.getElementById(canvas_id).getContext("2d");
    var data;
    try {
        data = JSON.parse(sdata);
    } catch (err) {
        console.log(err.message);
        return;
    }
    var config = {
        type: 'line',
        data: data,
        options: options_line
    };
    var myChart = new Chart(ctx, config);
    return myChart;
}