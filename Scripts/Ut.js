﻿function formato_moneda(valor) {
    var val = "", ret = "";
    val += typeof valor == "number" ? Math.round(valor) : valor;
    for (var i = val.length - 1; i >= 0; i--) {
        ret = ((val.length - 1 - i) % 3 == 2 && i > 0 ? "." : "") + val[i] + ret;
    }
    return ret;
}
function date_diff(date1, date2) {
    var timeDiff = Math.abs(date2.getTime() - date1.getTime());
    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
    return diffDays;
}

function json_to_table(json_object, table, row_id, columns) {
    var keys = [];
    var row, cell, th;
    row = table.insertRow(-1);
    if (columns == null) {
        for (var key in json_object[0]) {
            keys.push(key);
            cell = document.createElement("th");
            cell.row_id = key;
            cell.innerHTML = key;
            row.appendChild(cell);
            if (key == row_id) {
                cell.style.display = "none";
            }
        }
    } else {
        var key;
        for (var i = 0; i < columns.length; i++) {
            key = columns[i];
            keys.push(key);
            cell = document.createElement("th");
            cell.row_id = key;
            cell.innerHTML = key;
            row.appendChild(cell);
            if (key == row_id) {
                cell.style.display = "none";
            }
        }
    }
    var len = json_object.length, i = 0, klen = keys.length, j = 0;
    var dic = {};
    for (i = 0; i < len; i++) {
        row = table.insertRow(-1);
        for (j = 0; j < klen; j++) {
            cell = row.insertCell(-1);
            cell.innerHTML = json_object[i][keys[j]];
            if (keys[j] == row_id) {
                row[row_id] = json_object[i][keys[j]];
                dic[row[row_id]] = i + 1;
                cell.style.display = "none";
            }
        }
    }
    return dic;
}