﻿function isNumeric(str) {
    var pat = /^[0-9]+$/g;
    return pat.test(str);
}
function isEmail(str) {
    var pat = /^([a-z 0-9\\._-]+@[a-z 0-9_-]+[\\.]+[a-z]+)$/g;
    return pat.test(str);

}
function isDate(str) {
    try {
        var data = str.split("/");
        if (data.length != 3) return false;
        var fecha = data[2] + "-" + data[1] + "-" + data[0];
        var dd = parseInt(data[0]), mm = parseInt(data[1]), yy = parseInt(data[2]);
        var cmp = new Date(fecha);
        return dd = cmp.getUTCDate() == dd && cmp.getUTCMonth() == mm - 1 && cmp.getUTCFullYear() == yy;
    }
    catch (e) {
        return false;
    }
}
function removeZeros(control) {
    var text = control.value;
    text = text.replace(/^[0]*/, "");
    control.value = text;
}
function transformNumeric(control) {
    var text = control.value;
    if (!isNaN(text)) return;
    text = text.replace(/\./g, "");
    text = text.replace(",", ".");
    text = text.replace(/[^0-9 \.]/g, "");
    while (text.lastIndexOf(".") != text.indexOf(".")) {
        text = text.replace(/,/, "");    
    }
    control.value = isNaN(text)? "": text;
}
function validar_password(str) {
    var pat = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}/g;
    return pat.test(str);
}
function jsDecimals(e, step) {

    var evt = (e) ? e : window.event;
    var key = (evt.keyCode) ? evt.keyCode : evt.which;
    if (key != null) {
        key = parseInt(key, 10);
        if ((key < 48 || key > 57) && (key < 96 || key > 105)) {
            if (!jsIsUserFriendlyChar(key, step)) {
                return false;
            }
        }
        else {
            if (evt.shiftKey) {
                return false;
            }
        }
    }
    return true;
}
function jsIsUserFriendlyChar(val, step) {
    // Backspace, Tab, Enter, and Delete
    if (val == 8 || val == 9 || val == 45 || val == 46) {
        return true;
    }
    // Ctrl, Alt, CapsLock, Home, End, and Arrows  
    if ((val > 16 && val < 21) || (val > 34 && val < 41)) {
        return true;
    }
    if (step == "SI") {
        if (val == 111) {  //Slash
            return true;
        }
    }
    return false;
}