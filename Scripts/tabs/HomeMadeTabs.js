﻿function Tabs(prefix, initial, minHeight) {
    this.headers = document.querySelectorAll("div[id^=" + prefix + "-header-" + "]");
    this.selected = null;
    var content;
    var containter = this;
    var tabId;
    for (var i = 0; i < this.headers.length; i++) {
        tabId = this.headers[i].id.split("-")[2];
        this.headers[i].tabId = tabId;
        content = document.getElementById(prefix + "-content-" + tabId);
        if (minHeight != null)
            content.style.minHeight = minHeight;
        this.headers[i].content = content;
        if (content != null)
            content.style.display = tabId == initial ? "block" : "none";
        this.headers[i].addEventListener("click", function () {
            containter.selectTab(this);
        });
        if (tabId == initial)
            this.selectTab(this.headers[i]);
    }
}
Tabs.prototype.selectTab = function (tab) {
    if (this.selected != null) {
        this.selected.classList.remove("tabSelected");
        if (this.selected.content != null)
            this.selected.content.style.display = "none";
    }
    this.selected = tab;
    this.selected.classList.add("tabSelected");
    if (this.selected.content != null)
        this.selected.content.style.display = "block";

};
