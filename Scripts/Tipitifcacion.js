﻿function cargar_tipificacion(tip, dd1, dd2, dd3) {
    try {
        cargar_dd_tipificacion(dd1, tip);
        if (dd3 != null) {
            dd1.onchange = function () { cambio_tipitificacion(dd1, tip, dd3) };
            dd3.onchange = function () { cambio_tipitificacion(dd3, tip, dd2) };
        } else dd1.onchange = function () { cambio_tipitificacion(dd1, tip, dd2) };
        
        
    }
    catch (e) {
        mostrar_mensaje(e.message);
    }
}
function cambio_tipitificacion(dd_origen, tip_origen, dd_destino) {
    try {
        var id = dd_origen.value;
        var tip_destino = buscar_tip_destino(id, tip_origen);
        /*for (var i = 0; i < tip_origen.length; i++)
            if (tip_origen[i].id == id) {
                tip_destino = tip_origen[i].hijos;
                break;
            }*/
        if (tip_destino == null) mostrar_mensaje("Error - Tipificacion no encontrada");
        cargar_dd_tipificacion(dd_destino, tip_destino);
    } catch (e) {
        mostrar_mensaje(e.message);
    }
}
function buscar_tip_destino(id, tip) {
    var rtip = null;
    for (var i = 0; i < tip.length; i++)
        if (tip[i].id == id) {
            return tip[i].hijos;
        } else if ((rtip = buscar_tip_destino(id, tip[i].hijos)) != null)
            return rtip;
    return null;
}
function cargar_dd_tipificacion(dd, opt) {
    try {
        var i;
        for (i = dd.options.length - 1; i >= 0; i--)
            dd.remove(i);

        if (opt == null) return;

        var el = document.createElement("option");
        el.textContent = "[Seleccione uno]";
        el.value = "0";
        dd.appendChild(el);
        for (var i = 0; i < opt.length; i++) {
            var el = document.createElement("option");
            el.textContent = opt[i].valor;
            el.value = opt[i].id;
            dd.appendChild(el);
        }
    } catch (e) {
        mostrar_mensaje(e.message);
    }
}