﻿function httpCallObj(http, url, pars, succes, error) {
    http.post(url, pars).then(
        function (data, status, headers, config) {
            try {
                var obj;
                if (typeof data.data.d === 'string' || data.data.d instanceof String)
                    obj = JSON.parse(data.data.d);
                else obj = data.data.d;
                succes(obj);
            } catch (e) {
                ocultar_progreso();
                mostrar_mensaje_ng(e);
            }
        }
    ,
        function (data, status, headers, config) {
            ocultar_progreso();
            if (error == null)
                mostrar_mensaje_ng(data);
            else error(data);
        }
    );
}
function httpCallStr(http, url, pars, succes, error) {
    http.post(url, pars).then(
        function (data, status, headers, config) {
            succes(data.data.d);
        }
    ,
        function (data, status, headers, config) {
            ocultar_progreso();
            if (error == null)
                mostrar_mensaje_ng(data);
            else error(data);
        }
    );
}
function objectToTable(obj, size, exclude) {
    var ret = [];
    var pars = [];
    var vals = [];
    var i = 0;
    for (key in obj) {
        if (exclude != null && exclude.indexOf(key) >= 0) continue;
        pars.push({ key:key, value: key });
        vals.push({key:key, value: obj[key]});
        i++;
        if (i >= size) {
            ret.push(pars);
            ret.push(vals);

            pars = [];
            vals = [];
            i = 0;
        }
    }
    if (i > 0) {
        ret.push(pars);
        ret.push(vals);
    }
    return ret;
}