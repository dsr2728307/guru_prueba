var days = [];
var date_map = {};
var start_date = new Date();
var month = start_date.getUTCMonth()+1;
var year = start_date.getUTCFullYear();
var events =[];
var clickFunction;
function load_calendar(divd, dayClick){
	clickFunction = dayClick;
    var table = document.createElement("table");
    table.className = "calendar";
    var tbody = document.createElement("tbody");
    table.appendChild(tbody);
    var tr, td, div, span, day, div_01;
    //-------------------- Botones--- ------------------------
        tr = document.createElement("tr");
        td = document.createElement("td");
        td.colSpan = 2;
        td.innerHTML = "<button  onclick='return next_month(-1);'>Anterior</button>";
        tr.appendChild(td);
        td = document.createElement("td");
        td.colSpan = 3;
        td.innerHTML = "<div style='width:100%;' id='calendar_label_calendar'></div>";
        tr.appendChild(td);
        td = document.createElement("td");
        td.colSpan = 2;
        td.innerHTML = "<button onclick='return next_month(1);'>Siguiente</button>";
        tr.appendChild(td);
        tbody.appendChild(tr);    
    //--------------------------------------------------------
    //-------------------- Encabezado ------------------------
    tr = document.createElement("tr");
    tbody.appendChild(tr);
    for(var i = 0; i < 7; i++){
        td = document.createElement("th");
        td.innerHTML = date_name(i);
        tr.appendChild(td);
    }
    //--------------------------------------------------------
    for(var i = 0; i < 6; i++){
        tr = document.createElement("tr");
        tbody.appendChild(tr);
        for(var j = 0; j < 7; j++){
            td = document.createElement("td");
            div = document.createElement("div");
            div_01 = document.createElement("div");
            div_01.className = "llamadas";
            span = document.createElement("span");
            div.appendChild(span);
            div.appendChild(div_01);
            td.appendChild(div);
            tr.appendChild(td);
            day = {
                i: i,
                j: j,
                day: i*6 + j,
                date: null,
                td: td,
                span: span,
                div: div,
                div_01: div_01,
                events: []
            }
            days.push(day); 
        }
    }
    divd.appendChild(table);
    load_date(month, year);
    selectDate(map_key(year, month,(new Date()).getUTCDate()),month, year);
}
function load_date(month, year){
    this.month = month;
    this.year = year;
    date_map = {};
    document.getElementById("calendar_label_calendar").innerText = month_name(month)+" - "+year;
    var date = first_date_month(month, year);
    var rdate = first_date_month(month, year);
    date_add(rdate, -1*rdate.getUTCDay());
    var day_name = 1;
    for(var i = 0; i < days.length; i++){
        days[i].events = [];
        days[i].div_01.innerHTML = "";
		days[i].div.calendarData = null;
		days[i].div.day = map_key(year, month,day_name);
		days[i].div.onclick = selectDay;
		days[i].div.month = month;
		days[i].div.year = year;
        if(date.getUTCDay() == days[i].j && date.getUTCMonth() == month - 1){
            days[i].date = date;
            days[i].span.innerHTML = day_name + "";
            map_date(year, month,day_name, days[i]);
            day_name ++;
            date_add(date, 1);
            days[i].div.className = "day";
			
        }else {
            days[i].date = null;
            days[i].span.innerHTML = "";
            days[i].div.className = "no_day";
            if(date.getUTCMonth() > month - 1){
                days[i].span.innerHTML = date.getUTCDate() + "";
                map_date(year, month + 1,date.getUTCDate(), days[i]);
                date_add(date, 1);
            }else {
                days[i].span.innerHTML = rdate.getUTCDate() + "";
                map_date(year, month - 1,rdate.getUTCDate(), days[i]);
                date_add(rdate, 1);
            }
        }
    }
    load_events(null);
    if (selectedDay != null && selectedMonth != null && (selectedDay.div.month != selectedMonth.month || selectedDay.div.year != selectedMonth.year))
        selectedDay.div.style.background = null;
    else if (selectedDay != null && selectedMonth != null)
        selectedDay.div.style.background = "#ccffcc";
}
function map_date(year, month, day, div){
    var key = year+""+(month<10?"0":"")+month+""+(day<10?"0":"")+day;
    date_map[key] = div;
}
function map_key(year, month, day){
	return year+""+(month<10?"0":"")+month+""+(day<10?"0":"")+day;
}
function days_in_month(month, year){
    return new Date(year, month, 0).getDate();
}
function date_add(date,days){
    date.setDate(date.getDate() + days);
}
function first_date_month(month, year){
    return new Date(year, month - 1, 1);
}
function next_month(diff){
    var d = first_date_month(month + diff, year);
    load_date(d.getUTCMonth() + 1, d.getUTCFullYear());
    return false;
}
function date_name(week_day){
    if(week_day==0)
        return "Do";
    else if(week_day==1)
        return "Lu";
    else if(week_day==2)
        return "Ma";
    else if(week_day==3)
        return "Mi";
    else if(week_day==4)
        return "Ju";
    else if(week_day==5)
        return "Vi";
    else if(week_day==6)
        return "S&aacute;";
}
function month_name(month){
    if(month==1)
        return "Enero";
    else if(month==2)
        return "Febrero";
    else if(month==3)
        return "Marzo";
    else if(month==4)
        return "Abril";
    else if(month==5)
        return "Mayo";
    else if(month==6)
        return "Junio";
    else if(month==7)
        return "Julio";
    else if(month==8)
        return "Agosto";
    else if(month==9)
        return "Septiembre";
    else if(month==10)
        return "Octubre";
    else if(month==11)
        return "Noviembre";
    else if(month==12)
        return "Diciembre";
}
function load_events(levents) {
    if (levents != null) {
        events = levents;
        load_date(this.month, this.year);
        return;
    }
    var cday;
    for(var i = 0; i < events.length; i++){
        cday = date_map[events[i].date_key];
        if(cday!=null && events[i].month == this.month && events[i].year == this.year){
            cday.events.push(events[i]);
            cday.div_01.innerHTML ="&#9888;"+ cday.events.length; 
			if(cday.div.calendarData == null)
				cday.div.calendarData = [];
			cday.div.calendarData.push(events[i]);
			cday.div.day = map_key(events[i].year, events[i].month,events[i].day);
        }
    }
}
function selectDay(sender){
    selectDate(sender.currentTarget.day, sender.currentTarget.month, sender.currentTarget.year);
}
var selectedDay = null;
var selectedMonth = null;
function selectDate(date_key, month, year) {
    if (selectedDay != null)
        selectedDay.div.style.background = null;
    selectedDay = date_map[date_key];
    if (selectedDay == null) {
        load_date(month, year);
        selectedDay = date_map[date_key];
    }
    if (selectedDay !=null) {
        selectedDay.div.style.background = "#ccffcc";
        selectedMonth = { month: month, year: year };
        clickFunction(selectedDay.div.day, selectedDay.div.calendarData);
    }
}
function raiseDayClick() { 
    if(selectedDay != null)
        clickFunction(selectedDay.div.day, selectedDay.div.calendarData);
}
function fn_DateCompare(DateA) {     // this function is good for dates > 01/01/1970

    var a = new Date(DateA);
    var b = new Date();

    var msDateA = Date.UTC(a.getFullYear(), a.getMonth() + 1, a.getDate(), a.getHours() ,a.getMinutes());
    var msDateB = Date.UTC(b.getFullYear(), b.getMonth() + 1, b.getDate(), b.getHours() ,b.getMinutes());

    if (parseFloat(msDateA) < parseFloat(msDateB))
        return -1;  // lt
    else if (parseFloat(msDateA) == parseFloat(msDateB))
        return 0;  // eq
    else if (parseFloat(msDateA) > parseFloat(msDateB))
        return 1;  // gt
    else
        return null;  // error
}